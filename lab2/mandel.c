#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"

typedef struct {
   double real; 
   double imag;
} complex;
#define MAX_PE 512
#define MAX_PRINT 50
int cal_pixel(complex);

int main(int argc, char* argv[]) {
    int Nx, Ny, Nexpt;
    double Rx_min, Rx_max, Ry_min, Ry_max;
    double t1,t2,tseq, ttsk, tpar, ttsk_all[MAX_PE];
    int rank, size;
    int iexpt, i, x, y, ierr;
    int *pix_seq, *pix_tmp, *pix_par;
    int idata[3];
    double data[4];
    complex c;

    MPI_Init( &argc, &argv );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    MPI_Comm_size( MPI_COMM_WORLD, &size );
    MPI_Barrier(MPI_COMM_WORLD);

    /*  INPUT PROCESSING */
    if (!rank){
        for (i=0;i<72;i++)printf("#");
        printf("\n# COMP4300 Lab2 Mandelbrot Program\n");
        printf("# Number of processors %d\n",size);
        printf("# Input number of X and Y data points \n");
        scanf("%d%d",&Nx,&Ny);
        printf("# Input Rx_min and Rx_max \n");
        scanf("%lf%lf",&Rx_min,&Rx_max);
        if ( Rx_min < -2 || Rx_max > 2 ){
            fprintf(stderr, " Rx_min < -2 or Rx_max > +2 %lf %lf\n",Rx_min,Rx_max);
            MPI_Abort(MPI_COMM_WORLD, -1);
        }
        printf("# Input Ry_min and Ry_max \n");
        scanf("%lf%lf",&Ry_min,&Ry_max);
        if ( Ry_min < -2 || Ry_max > 2 ){
            fprintf(stderr, " Ry_min < -2 or Ry_max > +2 %lf %lf\n",Ry_min,Ry_max);
            MPI_Abort(MPI_COMM_WORLD, -1);
        }
        printf("# Input Number of Timing Experiments \n");
        scanf("%d",&Nexpt);
        for (i=0;i<72;i++)printf("#");
        printf("\n");
        printf("# Nx      %8d Ny     %8d \n",Nx, Ny);
        printf("# Rx_min  %8.2lf Rx_max %8.2lf \n", Rx_min, Rx_max);
        printf("# Ry_min  %8.2lf Ry_max %8.2lf \n", Ry_min, Ry_max);
        printf("# Nexpt   %8d\n",Nexpt);
        for (i=0;i<72;i++)printf("#");
        printf("\n");
        idata[0]=Nx;idata[1]=Ny;idata[2]=Nexpt;
        MPI_Bcast(idata, 3, MPI_INT, 0, MPI_COMM_WORLD);
        data[0]=Rx_min;data[1]=Rx_max;data[2]=Ry_min;data[3]=Ry_max;
        MPI_Bcast(data, 4, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    } else {
        MPI_Bcast(idata, 3, MPI_INT, 0, MPI_COMM_WORLD);
        Nx=idata[0];Ny=idata[1];Nexpt=idata[2];
        MPI_Bcast(data, 4, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        Rx_min=data[0];Rx_max=data[1];Ry_min=data[2];Ry_max=data[3];
    }

 /***********************************************************************/
 /*                    MAIN  COMPUTE PART                               */
 /***********************************************************************/
    /* Allocate data array */
    pix_seq  = (int*) malloc( Nx*Ny*sizeof(int) );
    pix_tmp  = (int*) malloc( Nx*Ny*sizeof(int) );
    pix_par  = (int*) malloc( Nx*Ny*sizeof(int) );

    for (iexpt = 0; iexpt < Nexpt; iexpt++){
    /* Compute pixels sequentially */
        t1 = MPI_Wtime();
        for (y = 0; y < Ny; y++) {
            for (x = 0; x < Nx; x++){
                 c.real = Rx_min + ((double) x * (Rx_max - Rx_min)/(double) (Nx - 1));
                 c.imag = Ry_min + ((double) y * (Ry_max - Ry_min)/(double) (Ny - 1));
                 pix_seq[y*Nx+x] = cal_pixel(c);
            }
        }
        t2 = MPI_Wtime() - t1;
        if (iexpt == 0) tseq = t2;
        tseq = (t2 < tseq) ? t2 : tseq;
  
        /* Compute pixels in parallel */

        MPI_Barrier(MPI_COMM_WORLD);
        t1 = MPI_Wtime();
        for (i = 0; i < Nx*Ny; i++)pix_tmp[i]=0.0;

        for (y = 0; y < Ny; y++){
            if (y%size == rank){
               for (x = 0; x < Nx; x++){
                 c.real = Rx_min + ((double) x * (Rx_max - Rx_min)/
                                                 (double) (Nx - 1));
                 c.imag = Ry_min + ((double) y * (Ry_max - Ry_min)/
                                                 (double) (Ny - 1));
                 pix_tmp[y*Nx+x] = cal_pixel(c);
               }
            }
        }
        MPI_Reduce(pix_tmp, pix_par, Nx*Ny, MPI_INT, 
                 MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);
        t2 = MPI_Wtime() - t1;

        if (iexpt == 0) tpar = t2;
        tpar = (t2 < tpar) ? t2 : tpar;
    }
    if (!rank){
        printf("# Sequential time %8.4lf\n",tseq);
        printf("# Parallel   time %8.4lf\n",tpar);
    }
 /***********************************************************************/
 /*                      END COMPUTE PART                               */
 /***********************************************************************/

    if (!rank){
        /* Check that results agree */
        ierr=0;
        for (y = 0; y < Ny; y++) {
            for (x = 0; x < Nx; x++) {
                if (pix_seq[y*Nx+x] != pix_par[y*Nx+x]){
                    fprintf(stderr, " Error %d %d %d %d \n",x,y,pix_seq[y*Nx+x],pix_par[y*Nx+x]);
                    ierr++;
                    if (ierr > 10)  MPI_Abort(MPI_COMM_WORLD, -1);
                }
            }
        }

        if (!ierr){
            printf("# All pixels appear to be correct!\n");
        } else { 
            MPI_Abort(MPI_COMM_WORLD, -1);
        }

        /* Print the pixels if few! */
        if ( Nx <= MAX_PRINT && Ny <= MAX_PRINT) {
            for (x = 0; x < Nx; x++){
                for (y = 0; y < Ny; y++){
                  printf("%5d", pix_seq[y*Nx+x]);
                }
                printf("\n");
            }
            printf("\n");
        }
    }

    free(pix_seq);
    free(pix_par);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize( );
    return 0;
}

int cal_pixel(complex c){
    int count, max, i;
    complex z;
    double temp, lengthsq, xx;
    max = 256;
    z.real = 0.0; z.imag = 0.0; 
    count = 0;
    do {
        temp = z.real * z.real - z.imag * z.imag + c.real;
        z.imag = 2.0 * z.real * z.imag + c.imag;
        z.real = temp;
        lengthsq = z.real * z.real + z.imag * z.imag;
        count++;
    } while ((lengthsq < 4.0) && (count < max));

    /* Add some random work related to count just to slow it down! */
    xx=1.0;
    for (i = 0; i < (count*count)%10000; i++) {
        if (xx > 0.0 && xx < 1294.0) {
            xx = xx + sqrt(xx)/(xx+ sqrt(xx)/xx);
        } else {
            xx = 1.0;
        }
    }
    if (xx < 0.0) count = -1; // This should never happen!
    /* End of random work */

    if (count == max) return 0;
    else return count;
}
 

