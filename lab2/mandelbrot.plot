set term x11 
set output 'mandelbrot.png'

set notics
set nokey
unset colorbox

set palette defined (0 "black", 1 "white", 255 "white")
plot 'mandelbrot.dat' u 2:1:3 matrix with image
pause mouse key
