#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"

#define MAX_PRINT 20

int main(int argc, char* argv[]) {
   int Nx, Ny, Max_iter;
   double Tedge, converge;
   MPI_Status status;
   int rank, size;
   int i, j, iter, chk, jst, jfin;
   double *tnew, *told, tdiff, lmxdiff, mxdiff;
   double t1,tpar;
   int idata[3];
   double data[2];

   MPI_Init( &argc, &argv );
   MPI_Comm_rank( MPI_COMM_WORLD, &rank );
   MPI_Comm_size( MPI_COMM_WORLD, &size );
   MPI_Barrier(MPI_COMM_WORLD);

 /*  INPUT PROCESSING */
   if (!rank){
      printf("\n COMP4300 Lab2 Hot Plate Program\n");
      printf(" Number of processors %d\n",size);
      printf(" Input number of X and Y data points \n");
      if (scanf("%d%d",&Nx,&Ny) != 2) {
        fprintf(stderr, "error: invalid X, Y\n");
        exit(1);
      }
      printf(" Input temperatures along edges \n");
      if (scanf("%lf",&Tedge) != 1) {
        fprintf(stderr, "error: invalid temperature\n");
        exit(1);
      }
      printf(" Input maximum iterations \n");
      if (scanf("%d",&Max_iter) != 1) {
        fprintf(stderr, "error: invalid X, Y\n");
        exit(1);
      }
      printf(" Input convergence \n");
      if (scanf("%lf",&converge) != 1) {
        fprintf(stderr, "error: invalid X, Y\n");
        exit(1);
      }

      for (i=0;i<72;i++) printf("-");
      printf("\n");
      printf(" Nx       %8d Ny     %8d \n",Nx, Ny);
      printf(" Tedge    %8.2lf\n", Tedge);
      printf(" Max_iter %8d\n", Max_iter);
      printf(" Converge %8.2lf\n",converge);
      for (i=0;i<72;i++) printf("-");
      printf("\n");

      idata[0]=Nx;idata[1]=Ny;idata[2]=Max_iter;
      MPI_Bcast(idata, 3, MPI_INT, 0, MPI_COMM_WORLD);
      data[0]=Tedge;data[1]=converge;
      MPI_Bcast(data, 2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
   } else {
      MPI_Bcast(idata, 3, MPI_INT, 0, MPI_COMM_WORLD);
      Nx=idata[0];Ny=idata[1];Max_iter=idata[2];
      MPI_Bcast(data, 2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
      Tedge=data[0];converge=data[1];
   }
   MPI_Barrier(MPI_COMM_WORLD);

/***********************************************************************/
/*                     MAIN COMPUTE PART                               */
/***********************************************************************/
    // Allocate data array

    told   = (double*) malloc( Nx*Ny*sizeof(double) );
    tnew   = (double*) malloc( Nx*Ny*sizeof(double) );

    /* *******Parallel Solution*********/
    if (!rank) printf("\n Begin Parallel Iterations\n");
    for (i = 0; i < Nx*Ny; i++){ told[i]=0.0;tnew[i]=0.0; }
    j=0;    for (i = 0; i < Nx; i++) told[j*Nx+i]=Tedge;
    j=Ny-1; for (i = 0; i < Nx; i++) told[j*Nx+i]=Tedge;
    i=0;    for (j = 0; j < Ny; j++) told[j*Nx+i]=Tedge;
    i=Nx-1; for (j = 0; j < Ny; j++) told[j*Nx+i]=Tedge;
    iter = 0; lmxdiff=0.0;
    chk=(Ny-2+size-1)/size;
    MPI_Barrier(MPI_COMM_WORLD);
    t1 = MPI_Wtime();
    MPI_Request request;
    do {
        iter++;

        // update local values
        jst = rank*chk+1;
        jfin = jst+chk > Ny-1 ? Ny-1 : jst+chk;
        for (j = jst; j < jfin; j++) {
            for (i = 1; i < Nx-1; i++) {
                tnew[j*Nx+i]=0.25*(told[j*Nx+i+1]+told[j*Nx+i-1]+told[(j+1)*Nx+i]+told[(j-1)*Nx+i]);
            }
        }

        // Send to rank+1
        if (rank+2 < size) {
            jst = rank*chk+chk;
            MPI_Isend(&tnew[jst*Nx],Nx, MPI_DOUBLE, rank+2, 
                                2, MPI_COMM_WORLD ,&request);
        }
        if (rank-2 >= 0) {
            jst = (rank-1)*chk+chk;
            MPI_Irecv(&tnew[jst*Nx],Nx, MPI_DOUBLE,rank-2, 
                                2, MPI_COMM_WORLD, &request);
        }

        // Send to rank-1
        if (rank-2 >= 0) {
            jst = rank*chk+1;
            MPI_Isend(&tnew[jst*Nx],Nx, MPI_DOUBLE, rank-2, 
                                1, MPI_COMM_WORLD, &request);
        }
        if (rank+2 < size) {
            jst= (rank+1)*chk+1;
            MPI_Irecv(&tnew[jst*Nx],Nx, MPI_DOUBLE, rank+2,
                                1, MPI_COMM_WORLD, &request);
        }

        // fix boundaries in tnew
        j=0;    for (i = 0; i < Nx; i++)tnew[j*Nx+i]=Tedge;
        j=Ny-1; for (i = 0; i < Nx; i++)tnew[j*Nx+i]=Tedge;
        i=0;    for (j = 0; j < Ny; j++)tnew[j*Nx+i]=Tedge;
        i=Nx-1; for (j = 0; j < Ny; j++)tnew[j*Nx+i]=Tedge;

        jst = rank*chk+1;
        lmxdiff = fabs( (double) (told[jst*Nx+1] - tnew[jst*Nx+1]));
        jfin = jst+chk > Ny-1 ? Ny-1 : jst+chk;
        for (j = jst; j < jfin; j++) {
            for (i = 1; i < Nx-1; i++) {
             tdiff = fabs( (double) (told[j*Nx+i] - tnew[j*Nx+i]));
             lmxdiff = (lmxdiff < tdiff) ? tdiff : lmxdiff;
            }
        }
        for (i = 0; i < Nx*Ny; i++) told[i]=tnew[i];

        MPI_Allreduce(&lmxdiff, &mxdiff, 1, MPI_DOUBLE, 
                MPI_MAX, MPI_COMM_WORLD);
        if (!rank) printf(" iteration %d convergence %lf\n",iter,mxdiff);
    } while ( mxdiff > converge && iter < Max_iter);

    MPI_Barrier(MPI_COMM_WORLD);
    tpar = MPI_Wtime() - t1;
    if (!rank) {
        printf("Total time parallel   %8.4lf\n",tpar);
    }

    /* Printout the result if small!*/
    if ( Nx < MAX_PRINT && Ny < MAX_PRINT && size == 1) {
        printf(" final grid values");
        for (j = 0; j < Ny; j++) {
            printf("\n Row %2d:   ",j);
            for (i = 0; i < Nx; i++) {
                printf("%10.2e", tnew[j*Nx+i]);
                if (i%5 == 4)printf("\n           ");
            }
        }
    }
    printf("\n");

/***********************************************************************/
/*                    End MAIN COMPUTE PART                            */
/***********************************************************************/

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();

    return 0;
}
