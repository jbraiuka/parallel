#include <stdio.h>
#include <math.h>
#include "mpi.h"

#define A(I,J) a[(I) + ((J))* ( m)]
#define B(I,J) b[(I) + ((J))* ( k)]
#define C(I,J) c[(I) + ((J))* ( m)]

int mult_replicated(int m, int n, int k, double* a, double* b, double* c) {
    MPI_Status status;
    MPI_Barrier(MPI_COMM_WORLD);
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Calculate the number of columns in a per processor
    // m = number of rows in A
    // n = Number of columns in B
    // k = columns in a / rows in B
    double *mya;
    int *setup;
    setup = malloc(sizeof(int)*2); 
    int r;
    double *myb;
    mya = malloc(sizeof(double)*m*k);
    if (!rank){    
        int numberper, extra;
        numberper = n/size;
        extra = n%size;
        r = numberper;
        // // Broadcast the matrix b

        myb = malloc(sizeof(double)*numberper*k);
        memcpy(myb, b, sizeof(double)*numberper*k);  
        memcpy(mya, a, sizeof(double)*m*k);  
        MPI_Bcast(a,m*k, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        setup[0] = numberper;
        setup[1] = extra;
        MPI_Bcast(setup, 2, MPI_INTEGER, 0, MPI_COMM_WORLD);      
        printf("Passed The Broacast \n");
        printf("Node %d rows is : %d\n",rank,r);
        int i;
        int colssent = numberper;
        for (i=1;i<size;i++){
            int colssend = numberper;
            if (i<=extra){
                colssend+=1;
            }
            MPI_Send(&b[k*colssent], k*colssend, MPI_DOUBLE, i, i, MPI_COMM_WORLD);    
            colssent +=colssend;
        }
        
        // printf("Node %d Passed The send \n", rank);
        // printf("Fully completed A:\n");
        // int j;
        // for(i=0;i<m;i++){
        //     for (j=0;j<k;j++){
        //         printf("%f  ",a[i*k + j]);
        //     }
        //     printf("\n");
        // }
        // printf("Fully completed B:\n");
        // for(i=0;i<k;i++){
        //     for (j=0;j<n;j++){
        //         printf("%f  ",b[i*n + j]);
        //     }
        //     printf("\n");
        // }

    // Send each process its rows 
    }else{
        int i,j;
        MPI_Bcast(mya, m*k, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Bcast(setup, 2, MPI_INTEGER, 0, MPI_COMM_WORLD);      
        // printf("The Value at 0,0: %f\n", matrixb[(0) + ((0))* ( k)]);
      
        r = setup[0];
        if (rank<=setup[1]){
            r+=1;
        }
        printf("Node %d cools is : %d\n",rank,r);
        myb = malloc(sizeof(double)*r*k);
        printf("Node %d Passed The Malloc \n", rank);

        MPI_Recv(myb, k*r, MPI_DOUBLE, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        printf("Node %d Passed The recieve \n", rank);
        
    }

    int i,j;
    printf("Node %d A:\n",rank);
    for (j=0;j<m;j++){
        for(i=0;i<k;i++){
            printf("%f  ",mya[i*m + j]);
        }
        printf("\n");
    }


    printf("Node %d B:\n",rank);
    
    for (j=0;j<r;j++){
        for(i=0;i<k;i++){
            printf("%f  ",myb[i*k + j]);
        }
        printf("\n");
    }
    int l;
    double * myc = malloc(sizeof(double)*k*m);
    // For every row
    for (i=0;i<r;i++){
        // For every column of B and C
        for (j=0;j<m;j++){
            // Set our 0 Value
            myc[i*n + j] = 0.0;

            for(l=0;l<k;l++){
                printf("My C %d is adding %d * %d\n", (i*n +j), (l*m +j),(i*k +l));
                // myc[i*n + j] += (myb[i*k + l] * mya[(l)*m +j ]);
            }
        }
    }

    printf("Node %d C:\n",rank);
    for (j=0;j<m;j++){
        for(i=0;i<r;i++){
            printf("%f  ",myc[i*r + j]);
        }
        printf("\n");
    }
    // Each process starts its calculations


    MPI_Barrier(MPI_COMM_WORLD);

    if (!rank){
        // memcpy(c, myc, sizeof(double)*m*r);
        for(i=1;i<size;i++){
            // 
        }
        // Recieve all the values back from the other processors
    }else{
        // Send your C data back to Processor 0
    }


        // distribute matrices A, B

    // replace with your distributed matrix multiplication code
    // for (i=0; i<m; i++) {
    //     for(j=0;j<n;j++) {
    //         C(i,j) = 0.0;
    //         for(l=0;l<k;l++) {
    //             C(i,j) += A(i,l) * B(l,j);
    //         }
    //     }
    // }

    // gather matrix C at process 0
    free(setup);
    free(mya);
    free(myb);
    free(myc);
    return 0;
}

/* Perform distributed matrix multiplication using provided code for the SUMMA algorithm. */
int mult_summa(int m, int n, int k, double* a, double* b, double* c) {
    // distribute matrices and create communicators
    // pdgemm(...);
    // gather matrix C at process 0
    return 0;
}



