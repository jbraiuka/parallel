#include <stdio.h>
#include <math.h>
#include "mpi.h"
#include <assert.h>

struct Grid {
    int rows;
    int cols;
};
/* 
 * Perform distributed matrix multiplication by replicating row panels of A
 * and column panels of B to each grid row/column in a PxQ grid of processes.
 */
int mult_replicated(int m, int n, int k, double* a, double* b, double* c) {
    MPI_Status status;
    MPI_Barrier(MPI_COMM_WORLD);
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Calculate # Of Columns Per Processor
    // m = number of rows in a
    // n = number of columns in MPI_Barrier
    // b = colunms in A / Rows in B

    // Declare the arrays for storing all of A, and part of b and c
    double *mya, *myb, *myc;
    mya = malloc(sizeof(double)*m*k);
    assert(mya!=NULL);
    int extra;

    // Number of Columns that this processor will be processing
    int r;

    if(!rank){
        // Store number of columns per row, and the extra rows it needs to distribute
        int numberper;
        //Calculate the column numbers and extra ones
        numberper = n/size;
        extra = n%size;
        r = numberper;

        //Store in node 0's copy of b and a
        myb = malloc(sizeof(double)*r*k);
        assert(myb!=NULL);
        void *ass;
        ass = memcpy(myb, b, sizeof(double)*r*k);
        assert(ass!=NULL);
        ass= memcpy(mya, a, sizeof(double)*m*k);
        assert(ass!=NULL);

        // Send the entire Matrix A
        MPI_Bcast(a,m*k, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        // Send each process its specific columns of B
        int i;
        int colsent = numberper;
        for(i=1;i<size;i++){
            int colsend = numberper;
            if(i<=extra){
                colsend +=1;
            }
            MPI_Send(&b[k*colsent], k*colsend, MPI_DOUBLE, i, i, MPI_COMM_WORLD);  
            colsent += colsend;
        }



        //free(setup);
    }else{
        // Setup our R Value
        //MPI_Bcast(setup, 2, MPI_INTEGER, 0, MPI_COMM_WORLD);
        int numberper;
        //Calculate the column numbers and extra ones
        numberper = n/size;
        extra = n%size;
        r = numberper;
        if(rank<=extra){
            r+=1;
        }
        //free(setup);

        // Recieve the entire matrix a
        MPI_Bcast(mya, m*k, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        // Recieve my specific portion of B
        myb = malloc(sizeof(double)*r*k);
        assert(myb!=NULL);
        MPI_Recv(myb, k*r, MPI_DOUBLE, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    }

    int i,j,l; 
    // Malloc in our array c
    myc = malloc(sizeof(double)*m*r);
    assert(myc!=NULL);
    char transa = 'N';
    char transb = 'N';
    double alpha = 1.0;
    double beta = 0.0;
    if (r>0){
        // Call DGEM if i have work to do
    dgemm_(&transa, &transb,
            &m, &r, &k,
            &alpha, mya, &m,
                    myb, &k,
            &beta, myc, &m);
    }

   if (!rank){
        void *ass;
        // Copy in my portion of C
        ass = memcpy(c, myc, sizeof(double)*m*r);
        assert(ass);
        int colrecvd = r;
        // Work out how many columns to recieve
        for(i=1;i<size;i++){
            int col_torecv = r;
            if(i<=extra){
                col_torecv +=1;
            }
            // MPI Recieve back portions of C
            MPI_Recv(&c[m*colrecvd], m*col_torecv, MPI_DOUBLE, i, MPI_ANY_TAG, MPI_COMM_WORLD, &status);  
            colrecvd += col_torecv;
        }
    }else{
        MPI_Send(myc, m*r, MPI_DOUBLE, 0, rank, MPI_COMM_WORLD); 
        // Send your C data back to Processor 0
    }
    MPI_Barrier(MPI_COMM_WORLD);
    free(mya);
    free(myb);
    free(myc);

    return 0;

}


//Function for assigning clusters of a matrix to a specific prossor. 
void assign_cluster(int m, int n,struct Grid *proc_grid, int * m_matrix, int* n_matrix){
    // Work out number of rows per processor
    int row_each = m / proc_grid->rows;
    int row_extra = m % proc_grid->rows;
    int each_row;
    // Assign said rows
    for (each_row=0;each_row<proc_grid->rows;each_row++){
        m_matrix[each_row] = (each_row<row_extra)? row_each +1:row_each;
    }
    // Work out number of columns per processor
    int col_each = n / proc_grid->cols;
    int col_extra = n % proc_grid->cols;
    // Assign said columns
    int each_col;
    for (each_col=0;each_col<proc_grid->cols;each_col++){
        n_matrix[each_col] = (each_col<col_extra)? col_each +1:col_each;
    }
}

// Function for calculating where a processors first entry is by row and column
void  *start(int m,int n, struct Grid *proc_grid, int my_row, int my_col, int *start_m, int *start_n){
    // Work out the piece size, and leftover
    int piece_size_m = m / proc_grid->rows;
    int leftover_m = m% proc_grid->rows;
    // Add in the extra values if we need
    if (my_row <leftover_m){ 
        *start_m = my_row * (piece_size_m + 1);
    }else{ 
        *start_m = my_row * piece_size_m + leftover_m;
    }
    // And same again for columns

    int piece_size_n = n / proc_grid->cols;
    int leftover_n = n% proc_grid->cols;
    if (my_col <leftover_n){ 
        *start_n = my_col * (piece_size_n + 1);
    }else{ 
        *start_n = my_col* piece_size_n + leftover_n;
    }
    return 0;
}


//Function for distrobuting Matrix A and Matrix B
double *distrubte_matrix(double *matrix, int m, int n, struct Grid *grid, int *m_matrix, int *n_matrix, int size, int rank){
    //Assign the cluster of matricies
    assign_cluster(m, n, grid, m_matrix, n_matrix);
    // Work out my location and malloc the memory
    int myrow, mycol;
    myrow = rank / grid->cols;
    mycol = rank % grid->cols;
    double *a_temp = malloc(m_matrix[myrow]*n_matrix[mycol]*sizeof(double));

    assert(a_temp!=NULL);
    // If we are process 0
    if (!rank) {
        // set up our variables
        int proc, start_row, start_col;
        for (proc=1; proc<size; proc++) {
            // Send out the various processor rows, 
            int p_row, p_col;
            p_row = proc / grid->cols;
            p_col = proc % grid->cols;
            start(m, n, grid, p_row, p_col, &start_row, &start_col);
            // By first copying into a buffer
            dlacpy_("General", &m_matrix[p_row], &n_matrix[p_col], &matrix[start_row+start_col*m], &m, a_temp, &m_matrix[p_row]);
            int count = m_matrix[p_row]*n_matrix[p_col];
            // Then send out the array
            MPI_Send(a_temp, count, MPI_DOUBLE, proc, 0, MPI_COMM_WORLD);
        }
        // Also copy into my own buffer
        dlacpy_("General", &m_matrix[0], &n_matrix[mycol], matrix, &m, a_temp, &m_matrix[0]);
    } else {
        // If we are not node 0, then recieve our values
        int count = m_matrix[myrow]*n_matrix[mycol];
        MPI_Recv(a_temp, count, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    return a_temp;
}

// Function for re-assembling the matrix
int assemble_matrix(double *c, double* c_result, int m, int n, struct Grid *grid, int *m_c, int *n_c, int size, int rank) {
    int myrow, mycol;
    myrow = rank / grid->cols;
    mycol = rank % grid->cols;

    // If we are process 0
    if (!rank) {
        // Copy in our stuff
        dlacpy_("General", &m_c[myrow], &n_c[mycol], c, &m_c[myrow], c_result, &m);
        int proc, proc_row, proc_col;
        int start_row, start_col;
        // For all the other processors
        for (proc=1; proc<size; proc++) {
            // Work out the start and end locations
            proc_row = proc / grid->cols;
            proc_col = proc % grid->cols;
            start(m, n, grid, proc_row, proc_col, &start_row, &start_col);
            // Send out the recieve values and copy them in
            MPI_Recv(c, m_c[proc_row]*n_c[proc_col], MPI_DOUBLE, proc, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            dlacpy_("General", &m_c[proc_row], &n_c[proc_col], c, &m_c[proc_row], &c_result[start_row+start_col*m], &m);
        }
    } else {
        MPI_Send(c, m_c[myrow]*n_c[mycol], MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }

    return 0;
}

/* Perform distributed matrix multiplication using provided code for the SUMMA algorithm. */
int mult_summa(int m, int n, int k, double* a, double* b, double* c) {
    // Setup our initial values
    int rank, size;
    int nb =100;
    double alpha = 1.0;
    double beta = 0.0;
    double *work1, *work2;

    // Work out our size and rank
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Setup our struct
    struct Grid grid;

    //Get the processor Grid setup
    int dims[2] = {0, 0};
    MPI_Dims_create(size, 2, dims);
    grid.rows = dims[0];
    grid.cols = dims[1];

    int myrow, mycol;
    //Get MY processor coordinates
    myrow = rank / grid.cols;
    mycol = rank % grid.cols;

    // create row and column communicators
    MPI_Comm comm_row, comm_col;
    MPI_Comm_split(MPI_COMM_WORLD, myrow, mycol, &comm_row); 
    MPI_Comm_split(MPI_COMM_WORLD, mycol, myrow, &comm_col);
    // Setup our arrays for storing work / processor
    int m_a[grid.rows];
    int n_a[grid.cols];
    // Distrobute out A
    a = distrubte_matrix(a, m, k, &grid, m_a, n_a, size, rank);

    // Distrobute out B
    int m_b[grid.rows];
    int n_b[grid.cols];
    b = distrubte_matrix(b, k, n, &grid, m_b, n_b, size, rank);

    // assign area for  C
    int m_c[grid.rows];
    int n_c[grid.cols];
    // Calculate the work that was done
    assign_cluster(m, n, &grid, m_c, n_c);

    double *c_result = c;
    c = malloc(m_c[myrow]*n_c[mycol]*sizeof(double));
    assert (c!=NULL);
    // allocate work arrays big enough to fit largest panel of A, B
    work1 = malloc(m_a[0]*n_a[0]*sizeof(double));
    assert(work1!=NULL);
    work2 = malloc(m_b[0]*n_b[0]*sizeof(double));
    assert(work2!=NULL);
    // Run PDGEM
    pdgemm(m, n, k, 100, 
           alpha, a, m_a[myrow], 
                b, m_b[myrow], 
           beta, c, m_c[myrow], 
           m_a, n_a, 
           m_b, n_b, 
           m_c, n_c, 
           comm_row, comm_col, 
           work1, work2);
    // Re-Assemble the matrix
    assemble_matrix(c, c_result, m, n, &grid, m_c, n_c, size, rank);

    free(a); free(b); free(c);
    free(work1);
    free(work2);

    return 0;

}