#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"
#include "ass1.h"
#include "summa.h"

#define ITERS 20

#define A(I,J) a[(I) + ((J))* ( m)]
#define B(I,J) b[(I) + ((J))* ( k)]
#define C(I,J) c[(I) + ((J))* ( m)]
#define D(I,J) d[(I) + ((J))* ( m)]

int compare_dgemm(double* a, double* b, double*c, int m, int n, int k);

int main(int argc, char *argv[]) {
    if (argc < 4) {
        fprintf(stderr, "Usage: ass1 M N K\n");
        exit(1);
    }
    int m = atoi(argv[1]);
    int n = atoi(argv[2]);
    int k = atoi(argv[3]);

    int rank, size;
    MPI_Init( &argc, &argv );
    MPI_Comm_size( MPI_COMM_WORLD, &size );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    if (!rank) printf("m %d n %d k %d\n", m, n, k);
    double *a = malloc(m*k*sizeof(double));
    double *b = malloc(k*n*sizeof(double));
    double *c = malloc(m*n*sizeof(double));
    int i, j;
    if (!rank) {
        for(i=0; i<m; i++) {
            for (j=0; j<k; j++) {
                A(i,j) = (double)(i+j);
            }
        }
        for(i=0; i<k; i++) {
            for (j=0; j<n; j++) {
                B(i,j) = (double)(i+j)-1;
            }
        }
        for(i=0; i<m; i++) {
            for (j=0; j<n; j++) {
                C(i,j) = 0.0;
            }
        }
    }
	double time_before, time_after, time_min = 0;
 	for(i=0;i<ITERS;i++){
		MPI_Barrier(MPI_COMM_WORLD);
		time_before = MPI_Wtime();
	    	mult_replicated(m, n, k, (double*)a, (double*)b, (double*)c);
		time_after = MPI_Wtime();
		double total_time = time_after - time_before;
		time_min = ((total_time < time_min)||(i==0))?total_time:time_min;
	}
    if (!rank) {
	    printf("Minimum time was %f for Replicated\n", time_min);
        printf("mult_replicated:     ");
        compare_dgemm((double*)a, (double*)b, (double*)c, m, n, k);
    }

    for(i=0;i<ITERS;i++){
        MPI_Barrier(MPI_COMM_WORLD);
        time_before = MPI_Wtime();
             mult_summa(m, n, k, (double*)a, (double*)b, (double*)c);
        time_after = MPI_Wtime();
        double total_time = time_after - time_before;
        time_min = ((total_time < time_min)||(i==0))?total_time:time_min;
    }
   
    if (!rank) {
        printf("Minimum time was %f for SUMMA\n", time_min);
        printf("mult_summa:           ");
        compare_dgemm((double*)a, (double*)b, (double*)c, m, n, k);
    }


    free(a); free(b); free(c);

    MPI_Finalize();
    return 0;
}

// compare matrix C=AB against DGEMM D=AB
int compare_dgemm(double* a, double* b, double*c, int m, int n, int k) {
    double *d = malloc(m*n*sizeof(double));
    char transa = 'N';
    char transb = 'N';
    double alpha = 1.0;
    double beta = 0.0;
    int i,j;
    int different = 0;
    dgemm_(&transa, &transb, 
           &m, &n, &k, 
           &alpha, a, &m, 
                   b, &k, 
           &beta,  d, &m);
    for (j=0; j<n; j++) {
        for(i=0; i<m; i++) {
            if(D(i,j) != C(i,j)) {
                fprintf(stderr, "Error: c[%d][%d] = %f d[%d][%d] = %f\n", i,j,c[j*m+i], i,j,d[j*m+i]);
                different = 1;
            }
        }
    }
    if (different) {
        printf("failed\n");
    } else {
        printf("passed\n");
    }

    free(d);

    return different;
}

