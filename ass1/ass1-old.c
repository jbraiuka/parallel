#include <stdio.h>
#include <math.h>
#include "mpi.h"
#include <assert.h>
int mult_replicated(int m, int n, int k, double* a, double* b, double* c) {
    MPI_Status status;
    MPI_Barrier(MPI_COMM_WORLD);
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Calculate # Of Columns Per Processor
    // m = number of rows in a
    // n = number of columns in MPI_Barrier
    // b = colunms in A / Rows in B

    // Declare the arrays for storing all of A, and part of b and c
    double *mya, *myb, *myc;
    mya = malloc(sizeof(double)*m*k);
    int extra;

    // Number of Columns that this processor will be processing
    int r;

    if(!rank){
        // Store number of columns per row, and the extra rows it needs to distribute
        int numberper;
        //Calculate the column numbers and extra ones
        numberper = n/size;
        extra = n%size;
        r = numberper;

        //Store in node 0's copy of b and a
        myb = malloc(sizeof(double)*r*k);
        assert(myb!=NULL);
        void *ass;
        ass = memcpy(myb, b, sizeof(double)*r*k);
        assert(ass!=NULL);
        ass= memcpy(mya, a, sizeof(double)*m*k);
        assert(ass!=NULL);

        // Send the entire Matrix A
        MPI_Bcast(a,m*k, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        // Send each process its specific columns of B
        int i;
        int colsent = numberper;
        for(i=1;i<size;i++){
            int colsend = numberper;
            if(i<=extra){
                colsend +=1;
            }
            MPI_Send(&b[k*colsent], k*colsend, MPI_DOUBLE, i, i, MPI_COMM_WORLD);  
            colsent += colsend;
        }



        //free(setup);
    }else{
        // Setup our R Value
        //MPI_Bcast(setup, 2, MPI_INTEGER, 0, MPI_COMM_WORLD);
        int numberper;
        //Calculate the column numbers and extra ones
        numberper = n/size;
        extra = n%size;
        r = numberper;
        if(rank<=extra){
            r+=1;
        }
        //free(setup);

        // Recieve the entire matrix a
        MPI_Bcast(mya, m*k, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        // Recieve my specific portion of B
        myb = malloc(sizeof(double)*r*k);
        assert(myb!=NULL);
        MPI_Recv(myb, k*r, MPI_DOUBLE, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    }

    int i,j,l; 
    // Malloc in our array c
    myc = malloc(sizeof(double)*m*r);
    assert(myc!=NULL);
    char transa = 'N';
    char transb = 'N';
    double alpha = 1.0;
    double beta = 0.0;
    if (r>0){
        // Call DGEM if i have work to do
    dgemm_(&transa, &transb,
            &m, &r, &k,
            &alpha, mya, &m,
                    myb, &k,
            &beta, myc, &m);
    }

   if (!rank){
        void *ass;
        // Copy in my portion of C
        ass = memcpy(c, myc, sizeof(double)*m*r);
        assert(ass);
        int colrecvd = r;
        // Work out how many columns to recieve
        for(i=1;i<size;i++){
            int col_torecv = r;
            if(i<=extra){
                col_torecv +=1;
            }
            // MPI Recieve back portions of C
            MPI_Recv(&c[m*colrecvd], m*col_torecv, MPI_DOUBLE, i, MPI_ANY_TAG, MPI_COMM_WORLD, &status);  
            colrecvd += col_torecv;
        }
    }else{
        MPI_Send(myc, m*r, MPI_DOUBLE, 0, rank, MPI_COMM_WORLD); 
        // Send your C data back to Processor 0
    }
    MPI_Barrier(MPI_COMM_WORLD);
    free(mya);
    free(myb);
    free(myc);

    return 0;

}

// Function for calculating which value we start our chunk of a matrix at
int start(int m,int *m_a, int *n_a, int *proc_grid, int *my_coord){
    int start = 0;
    int i=0;
    // While we aren't at my column yet, increase it
    while(i<my_coord[0]-1){
        start += m_a[i] * m;
        i+=1;
    }
    i=0;
    // While we arent at my row yet, increase it
    while(i<my_coord[1]-1){
        start += n_a[i];
        i+=1;
    }
    return start;
}

// Function for distrobuting a matrix amongst nodes
double *distribute_matrix(double *matrix, int m, int n, int *proc_dims, int *m_matrix, int *n_matrix, int rank, int size, int* my_coord){
    double * a_temp;
    if(!rank){
        int proc;
        for(proc = 1;proc<size;proc++){
            int start_value;
            int proc_row = proc / proc_dims[1];
            int proc_col = proc%proc_dims[1];
            //Set aside some space for the data we will recieve
            a_temp = malloc(m_matrix[proc_row] * n_matrix[proc_col] * sizeof(double));
            assert(a_temp!=NULL);
            // Calculate our start value
            int proc_coord[2];
            proc_coord[0] = proc_row;
            proc_coord[1] =  proc_col;
            start_value = start(m, m_matrix, n_matrix, proc_dims, proc_coord);
            // Work out how many elements we have to send, and copy them, then send them
            int elements_to_send = m_matrix[proc_row] * n_matrix[proc_col];
            dlacpy_("General", &m_matrix[proc_row], &n_matrix[proc_col], &matrix[start_value], &m, a_temp, &m_matrix[proc_row]);
            MPI_Send(a_temp, elements_to_send, MPI_DOUBLE, proc, 0, MPI_COMM_WORLD);
        }   
        // Copy in Node 0's Values
        a_temp = malloc(m_matrix[0] * n_matrix[0] * sizeof(double));
        dlacpy_("General", m_matrix, n_matrix, matrix, &m, a_temp, m_matrix);
    }else{
        // Set aside some memory for my portion of A
        a_temp = malloc(m_matrix[my_coord[0]] * n_matrix[my_coord[1]] * sizeof(double));
        int count = m_matrix[my_coord[0]]*n_matrix[my_coord[1]];
        // Recieve those values
        MPI_Recv(a_temp, count, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    }
    return a_temp;
}

// Used for re-assembling a matrix once its been calculated. 
int assemble_matrix(double *c, double *c_solution, int m, int n, int *proc_dims, int *m_c, int *n_c, int size, int rank, int* my_coord){
      if(!rank){
        // Copy in our values that node 0 calculated
        dlacpy_("General", &m_c[0], &n_c[0], c, &m_c[0], c_solution, &m);
        int proc;
        //  Iterate over our processors
        for (proc=1;proc<size;proc++){
            int proc_row = size / proc_dims[1];
            int proc_col = size % proc_dims[1];
            // Recieving the data
            MPI_Recv(c, m_c[proc_row]*n_c[proc_col], MPI_DOUBLE, proc, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            int proc_coord[2];
            proc_coord[0] = proc_row;
            proc_coord[1] =  proc_col;
            // Calculating where we are starting from
            int start_value = start(m, m_c, n_c, proc_dims, proc_coord);
            // AND IF IT WAS WORKING (But it doesnt) Copy into the solution
       	    //dlacpy_("General", &m_c[proc_row], &n_c[proc_col], c, &m_c[proc_row], &c_solution[start_value], &m);
        }
        
    }else{
        // Send our portion back to node 0
        MPI_Send(c, m_c[my_coord[0]]*n_c[my_coord[1]], MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }

    return 0;
}



// Function for assigning work to the vairious comlumns and rows of processors
void assign_cluster(int m, int n,int *proc_grid, int * m_matrix, int* n_matrix){
    int row_each = m / proc_grid[0];
    int row_extra = m% proc_grid[0];
    int each_row;
    m_matrix[0] = row_each;
    for (each_row=1;each_row<proc_grid[0];each_row++){
        m_matrix[each_row] = (each_row<=row_extra)? row_each +1:row_each;
    }

    int col_each = n / proc_grid[1];
    int col_extra = n % proc_grid[1];
    int each_col;
    n_matrix[0] = col_each;
    for (each_col=1;each_col<proc_grid[1];each_col++){
        n_matrix[each_col] = (each_col<=col_extra)? col_each +1:col_each;
    }
}

/* Perform distributed matrix multiplication using provided code for the SUMMA algorithm. */
int mult_summa(int m, int n, int k, double* a, double* b, double* c) {
    //MPI_Status status;
    //MPI_Barrier(MPI_COMM_WORLD);
    //int rank, size;
    //int nb = 1;
    //double alpha = 1.0;
    //double beta = 0.0;
    double *work1, *work2;
    double *mya, *myb;
    MPI_Comm comm_row, comm_col;

    // Get my size and rank
    //MPI_Comm_size(MPI_COMM_WORLD, &size);
    //MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Stores the dimensions of my processor grid, Rows, then columns
    int proc_dims[2] = {0,0};
    MPI_Dims_create(size,2,proc_dims);

    // Stores this specific processors address in the grid, row col
    int my_coord[2];
    my_coord[0] = rank / proc_dims[1];
    my_coord[1] = rank % proc_dims[1];

    // Setup the row and column communicators
    MPI_Comm_split(MPI_COMM_WORLD, my_coord[0], rank, &comm_row);
    MPI_Comm_split(MPI_COMM_WORLD, my_coord[1], rank, &comm_col);

    int m_a[proc_dims[0]];
    int n_a[proc_dims[1]];

    assign_cluster(m, k, proc_dims, m_a, n_a);
    a = distribute_matrix(a, m, k, proc_dims, m_a, n_a, rank, size, my_coord);
    int m_b[proc_dims[0]];
    int n_b[proc_dims[1]];
    //printf("Distrobuted A:\n");

    assign_cluster(k, n, proc_dims, m_b, n_b);
    b = distribute_matrix(b, k, n, proc_dims, m_b, n_b, rank, size, my_coord);
    //printf("Distrobuted B:\n");

    //printf("Distrobuted a and b \n");
    int m_c[proc_dims[0]];
    int n_c[proc_dims[1]];
    assign_cluster(m, n, proc_dims, m_c, n_c);
    double * c_solution = c;
    c_solution = malloc(m_c[my_coord[0]] * n_c[my_coord[1]] * sizeof(double));
    assert(c_solution!=NULL);
    work1 = malloc(m_a[0]*n_a[0]*sizeof(double));
    assert(work1!=NULL);
    work2 = malloc(m_b[0]*n_b[0]*sizeof(double));
    assert(work2!=NULL);

    // Call PDGEMM with my work
    pdgemm(m,n,k,100,1.0, a, m_a[my_coord[0]], b, m_b[my_coord[0]], c, m_c[my_coord[0]], m_a, n_a, m_b, n_b, m_c, n_c, comm_row, comm_col, work1, work2);

    //Bring the matrix back together
    assemble_matrix(c, c_solution, m, n, proc_dims, m_c, n_c, size, rank, my_coord);
    // Free up all the memory
    free(a);
    free(b);
    free(c_solution);
    free(work1);
    free(work2);
    return 0;
}
    
