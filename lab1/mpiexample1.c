#include <stdio.h>
#include "mpi.h"

int main( argc, argv )
int  argc;
char **argv;
{
    int rank, size;
    char node [56];
    MPI_Init( &argc, &argv );
    MPI_Comm_size( MPI_COMM_WORLD, &size );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    gethostname(node, sizeof(node));
    printf( "Hello world from process %d of %d, from node %s\n", rank, size, node );
    MPI_Finalize();
    return 0;
}

