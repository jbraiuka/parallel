#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#include <unistd.h>
int main( argc, argv )
int argc;
char **argv;
{
  int        rank, size, j, len;
  MPI_Status status;
  int     *sbuf,*rbuf;
  int tag0=100,tag1=200;

  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );
  printf("\n Hello from process %d \n",rank);

  len=1024;
  sbuf = malloc(len*sizeof(int));
  rbuf = malloc(len*sizeof(int));
  for (j=0; j<len; j++)sbuf[j]=rank;
    if (rank == 0){
    MPI_Send( sbuf, len, MPI_INTEGER, rank+1, tag0, MPI_COMM_WORLD );
    MPI_Recv( rbuf, len, MPI_INTEGER, rank+1, tag1, MPI_COMM_WORLD, &status );
  }
  else if (rank == 1){
    MPI_Recv( rbuf, len, MPI_INTEGER, rank-1, tag0, MPI_COMM_WORLD, &status );
    MPI_Send( sbuf, len, MPI_INTEGER, rank-1, tag1, MPI_COMM_WORLD);

  } 

  MPI_Barrier(MPI_COMM_WORLD);
  printf(" Rank %d First 5 elements of rbuf \n %d %d %d %d %d \n",
         rank,rbuf[0],rbuf[1],rbuf[2],rbuf[3],rbuf[4]);
  printf("Goodbye from process %d \n",rank);
  MPI_Barrier(MPI_COMM_WORLD);

  free(sbuf);
  free(rbuf);
  MPI_Finalize( );
  return 0;
}
