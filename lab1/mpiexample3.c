#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

int main( argc, argv )
int argc;
char **argv;
{
  int        rank, size, i, len;
  MPI_Status status;
  int     *mbuf;
  double t1,t2,timeMin,timeMax;

  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );

  len=1;
  mbuf = malloc(len*sizeof(int));
  for (i=0; i<100; i++){
    t1 = MPI_Wtime();
    if (rank == 0){
      MPI_Send( mbuf, len, MPI_INTEGER, rank+1, 0, MPI_COMM_WORLD );
      MPI_Recv( mbuf, len, MPI_INTEGER, rank+1, 1, MPI_COMM_WORLD, &status );
    }
    else if (rank == 1){
      MPI_Send( mbuf, len, MPI_INTEGER, rank-1, 1, MPI_COMM_WORLD);
      MPI_Recv( mbuf, len, MPI_INTEGER, rank-1, 0, MPI_COMM_WORLD, &status );
    } 
    t2 = MPI_Wtime()-t1;
    if (i == 0) {timeMin = t2; timeMax = t2;}
    timeMin = (t2 < timeMin) ? t2 : timeMin;
    timeMax = (t2 > timeMax) ? t2 : timeMax;
  }
  free(mbuf);
  printf("Minimum time %f\n",timeMin);
  printf("Maximum time %f\n",timeMax);

  MPI_Barrier(MPI_COMM_WORLD);

  MPI_Finalize( );
  return 0;
}
